// Import the library
const server = require('server');
const routers = require('router')

// Launch the server to always answer "Hello world"
server({port: 8080, security: {csrf: false}}, routers);