// routes.js
const { get, post } = require('server/router');
const { send, json } = require('server/reply');
const fs = require('fs')


const signup = ctx => {
    console.log(ctx.body)
    fs.appendFile('signups.txt', JSON.stringify(ctx.body) + '\n', (error) =>{
        if(error) {
            return send(error)
        }
    })
    return send('Your request has been saved.')
}

// You can simply export an array of routes
module.exports = [
  get('/', ctx => send('./public/index.html')),
  post('/signup', signup)
];